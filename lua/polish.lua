local vim = vim
-- This will run last in the setup process and is a good place to configure
-- things like custom filetypes. This just pure lua so anything that doesn't
-- fit in the normal config locations above can go here

-- Set up custom filetypes
vim.filetype.add {
  extension = {
    ["http"] = "http",
  },
}

-- {{{4 Autosave
vim.api.nvim_create_autocmd({ "FocusLost" }, {
  pattern = { "*" },
  callback = function() vim.cmd "silent! wa" end,
})
-- 4}}}
-- {{{4 defaultFiles
vim.api.nvim_create_augroup("defaultFiles", { clear = true })
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "REST HTTP Template",
  group = "defaultFiles",
  pattern = "requests.http",
  command = "0r $HOME/.config/nvim/lua/templates/requests.http",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Local nvimrc Template",
  group = "defaultFiles",
  pattern = ".nvim.lua",
  command = "0r $HOME/.config/nvim/lua/templates/nvim.lua",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Projectionist Template",
  group = "defaultFiles",
  pattern = ".projections.json",
  command = "0r $HOME/.config/nvim/lua/templates/projections.json",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "gitignore",
  group = "defaultFiles",
  pattern = ".gitignore",
  command = "0r $HOME/.config/nvim/lua/templates/.gitignore",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Dir Env Template",
  group = "defaultFiles",
  pattern = ".envrc",
  command = "0r $HOME/.config/nvim/lua/templates/envrc",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "SQL Template",
  group = "defaultFiles",
  pattern = ".scratch/database.sql",
  command = "0r $HOME/.config/nvim/lua/templates/scratch.sql",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "reloadignore Template",
  group = "defaultFiles",
  pattern = ".reloadignore",
  command = "0r $HOME/.config/nvim/lua/templates/reloadignore",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Makefile Template",
  group = "defaultFiles",
  pattern = "Makefile",
  command = "0r $HOME/.config/nvim/lua/templates/Makefile",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Gradle Build Template",
  group = "defaultFiles",
  pattern = "build.gradle.kts",
  command = "0r $HOME/.config/nvim/lua/templates/build.gradle.kts",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "pyproject.toml",
  group = "defaultFiles",
  pattern = "pyproject.toml",
  command = "0r $HOME/.config/nvim/lua/templates/pyproject.toml",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "CMakeLists.txt",
  group = "defaultFiles",
  pattern = "CMakeLists.txt",
  command = "0r $HOME/.config/nvim/lua/templates/CMakeLists.txt",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "main.cpp",
  group = "defaultFiles",
  pattern = "main.cpp",
  command = "0r $HOME/.config/nvim/lua/templates/main.cpp",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = ".nvim-dap.lua",
  group = "defaultFiles",
  pattern = ".nvim-dap.lua",
  command = "0r $HOME/.config/nvim/lua/templates/nvim-dap.lua",
})
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "compile_flags.txt",
  group = "defaultFiles",
  pattern = "compile_flags.txt",
  command = "0r $HOME/.config/nvim/lua/templates/compile_flags.txt",
})
-- 4}}}
-- python {{{4
vim.api.nvim_create_augroup("PythonDev", { clear = true })
vim.api.nvim_create_autocmd("BufRead", {
  desc = "Set Python filetype",
  group = "PythonDev",
  pattern = "*.py",
  callback = require("user.utils").DevelopmentSetup,
})
-- 4}}}
-- dbConsole {{{4
vim.api.nvim_create_augroup("dbConsole", { clear = true })
vim.api.nvim_create_autocmd("BufNewFile", {
  desc = "Set SQL filetype",
  group = "dbConsole",
  pattern = "*.sql",
  command = "set filetype=sql",
})
vim.api.nvim_create_autocmd("BufRead", {
  desc = "Set SQL filetype",
  group = "dbConsole",
  pattern = "*.sql",
  command = "set filetype=sql",
})
vim.api.nvim_create_autocmd("FileType", {
  desc = "DB Console",
  group = "dbConsole",
  pattern = "sql",
  callback = function()
    local bufnr = vim.api.nvim_get_current_buf()
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "V:DB<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":DB<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F3>", ":ExecAllBlocks<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F6>", ":ClearResults<CR>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "v", "<F2>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
    vim.opt_local.foldlevel = 3
  end,
})
-- 4}}}
local function reload_flexipatch(project_name, exe)
  if not exe then exe = project_name end
  return function()
    local cwd = vim.fn.getcwd()
    if string.find(cwd, project_name, 1, true) then
      vim.api.nvim_command(
        "silent !notify-send 'Reloading' '"
          .. project_name
          .. "'; cd "
          .. cwd
          .. "; rm -f config.h patches.h; sudo make clean install && { kill -HUP $(pidof "
          .. exe
          .. ")}"
      )
    end
  end
end
-- local-nvim {{{4
vim.api.nvim_create_augroup("local-nvim-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "local .nvim.lua Reload",
  group = "local-nvim-reload",
  pattern = "*/.nvim.lua",
  callback = function() vim.api.nvim_command ":silent trust|so %" end,
})
-- 4}}}
-- dusk {{{4
vim.api.nvim_create_augroup("dusk-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "dusk Reload",
  group = "dusk-reload",
  pattern = "*/dusk/config.def.h",
  callback = reload_flexipatch "dusk",
})
-- 4}}}
-- dwm {{{4
vim.api.nvim_create_augroup("dwm-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "dwm Reload",
  group = "dwm-reload",
  pattern = "*/dwm-flexipatch/*",
  callback = reload_flexipatch("dwm-flexipatch", "dwm"),
})
-- 4}}}
-- dmenu {{{4
vim.api.nvim_create_augroup("dmenu-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "dmenu Reload",
  group = "dmenu-reload",
  pattern = "*/dmenu-flexipatch/*",
  callback = reload_flexipatch("dmenu-flexipatch", "dmenu"),
})
-- 4}}}
-- st {{{4
vim.api.nvim_create_augroup("st-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "st Reload",
  group = "st-reload",
  pattern = "*/st-flexipatch/*",
  callback = reload_flexipatch("st-flexipatch", "st"),
})
-- 4}}}
-- disable lsp for *.def.h {{{4
vim.api.nvim_create_autocmd({ "VimEnter", "BufRead", "BufNewFile" }, {
  pattern = "*.def.h",
  callback = function() vim.diagnostic.enable(false) end,
})
-- 4}}}
-- aerospace {{{4
vim.api.nvim_create_augroup("aerospace-reload", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  desc = "aerospace Reload",
  group = "aerospace-reload",
  pattern = "aerospace.toml",
  callback = function()
    vim.api.nvim_command "silent !terminal-notifier -title Aerospace -message 'Aerospace Reloading'; aerospace reload-config"
  end,
})
-- 4}}}
-- scratch {{{4
vim.api.nvim_create_augroup("scratch", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
  desc = "Scratch files",
  group = "scratch",
  pattern = ".scratch.*",
  callback = function()
    local bufnr = vim.api.nvim_get_current_buf()
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "vip:ExecCodeBlock<cr>", { silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":ExecCodeBlock<cr>", { silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F3>", ":ExecAllBlocks<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F6>", ":ClearResults<CR>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
    vim.api.nvim_buf_set_keymap(bufnr, "v", "<F2>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
  end,
})
-- 4}}}
-- asm commentstring {{{4
vim.api.nvim_create_augroup("asm-commentstring", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
  desc = "ASM files",
  group = "asm-commentstring",
  pattern = "asm",
  callback = function() vim.api.nvim_set_option_value("commentstring", "; %s", { buf = 0 }) end,
})
-- 4}}}

require("user.code-blocks").setup {
  execs = {
    pio = { exec = "pio" },
    wibble = { exec = "echo" },
    dj = { exec = "duckdb", opts = "-json -c", prefix = "dj>" },
    dt = { exec = "duckdb", opts = "ticker.db -box -c", prefix = "dt>" },
    sql = { exec = "duckdb", opts = "-json -c", prefix = "sql>" },
    dm = { exec = "duckdb", opts = "-markdown -c", prefix = "dm>" },
  },
}

require("nvim-dap-projects").search_project_config()
require("user.global-marks").setup()
require "plugins.luasnip"

if vim.fn.has "mac" then require("conform").setup() end
vim.api.nvim_set_hl(0, "NotifyBackground", vim.api.nvim_get_hl_by_name("Normal", true))
