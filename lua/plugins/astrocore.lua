-- AstroCore provides a central place to modify mappings, vim options, autocommands, and more!
-- Configuration documentation can be found with `:h astrocore`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    -- Configure core features of AstroNvim
    features = {
      large_buf = { size = 1024 * 500, lines = 10000 }, -- set global limits for large files for disabling features like treesitter
      autopairs = true, -- enable autopairs at start
      cmp = true, -- enable completion at start
      diagnostics_mode = 3, -- diagnostic mode on start (0 = off, 1 = no signs/virtual text, 2 = no virtual text, 3 = on)
      highlighturl = true, -- highlight URLs at start
      notifications = true, -- enable notifications at start
    },
    -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
    diagnostics = {
      virtual_text = true,
      underline = true,
    },
    -- vim options can be configured here
    options = {
      opt = { -- vim.opt.<key>
        relativenumber = false, -- sets vim.opt.relativenumber
        number = false, -- sets vim.opt.number
        spell = false, -- sets vim.opt.spell
        signcolumn = "auto", -- sets vim.opt.signcolumn to auto
        wrap = false, -- sets vim.opt.wrap
        scrolloff = 3,
        guifont = "FiraCode Nerd Font Mono:h18",
        cmdheight = 1,
        exrc = true,
        showtabline = 0,
      },
      g = { -- vim.g.<key>
        -- configure global vim variables (vim.g)
        -- NOTE: `mapleader` and `maplocalleader` must be set in the AstroNvim opts or before `lazy.setup`
        -- This can be found in the `lua/lazy_setup.lua` file
      },
    },
    -- Mappings can be configured through AstroCore as well.
    -- NOTE: keycodes follow the casing in the vimdocs. For example, `<Leader>` must be capitalized
    mappings = {
      -- first key is the mode
      n = {
        -- second key is the lefthand side of the map

        -- navigate buffer tabs with `H` and `L`
        L = {
          function() require("astrocore.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end,
          desc = "Next buffer",
        },
        H = {
          function() require("astrocore.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end,
          desc = "Previous buffer",
        },

        -- mappings seen under group name "Buffer"
        ["<Leader>bD"] = {
          function()
            require("astroui.status.heirline").buffer_picker(
              function(bufnr) require("astrocore.buffer").close(bufnr) end
            )
          end,
          desc = "Pick to close",
        },
        -- Quickly toggle `diffthis`
        ["<Leader>dt"] = {
          function()
            if vim.api.nvim_win_get_option(0, "diff") then
              vim.api.nvim_exec(":windo diffoff", true)
            else
              vim.api.nvim_exec(":windo diffthis", true)
            end
          end,
          desc = "Toggle diffthis",
        },
        -- tables with just a `desc` key will be registered with which-key if it's installed
        -- this is useful for naming menus
        ["<Leader>b"] = { desc = "Buffers" },
        ["<left>"] = { "<cmd>tabp<cr>", desc = "Prev Tab" },
        ["<Right>"] = { "<cmd>tabn<cr>", desc = "Next Tab" },

        ["<Leader>="] = { "<cmd>Neotree buffers toggle<cr>", desc = "Neotree Buffers" },
        ["<Leader>G"] = { "<cmd>Neotree git_status toggle<cr>", desc = "Neotree Git" },
        ["<Leader>a"] = { desc = "Alternate" },
        ["<Leader>aa"] = { "<CMD>A<CR>", desc = "Open Alternate" },
        ["<Leader>as"] = { "<CMD>silent only|sp|A<CR>", desc = "Open Alternate in H-split" },
        ["<Leader>av"] = { "<CMD>silent only|vs|A<CR>", desc = "Open Alternate in V-split" },

        ["<Leader>m"] = { desc = "Make" },
        ["<Leader>mr"] = { "<cmd>wa|silent call VimuxRunCommand('make run')<cr>", desc = "Make Run" },
        ["<Leader>mc"] = { "<cmd>wa|silent call VimuxRunCommand('make clean')<cr>", desc = "Make Clean" },

        ["<Leader>r"] = { desc = "Remote Run" },
        ["<Leader>rb"] = { "<cmd>wa|silent call VimuxRunCommand('build')<cr>", desc = "Build" },
        ["<Leader>rc"] = {
          "<cmd>wa|silent call VimuxRunCommand('remote_run make clean')<cr>",
          desc = "Clean",
        },
        ["<Leader>rs"] = { "<cmd>wa|Start<cr>", desc = "Start service" },
        ["<Leader>rt"] = { "<cmd>wa|silent call VimuxRunCommand('test-all')<cr>", desc = "Run All Tests" },
        ["<Leader>R"] = { desc = "REST requests" },
        ["<Leader>Rr"] = { "<cmd>Rest run<cr>", desc = "Run request under the cursor" },
        ["<Leader>Rl"] = { "<cmd>Rest run last<cr>", desc = "Re-run latest request" },
        ["<Leader>v"] = { desc = "Vimux" },
        ["<Leader>vr"] = { ":VimuxRunCommand<space>", desc = "Vimux Run Command" },
        ["<Leader>vt"] = { "<cmd>VimuxTogglePane<cr>", desc = "Toggle Vimux Pane" },
        ["<Leader>vz"] = { "<cmd>VimuxZoomRunner<cr>", desc = "Zoom Vimux Pane" },
      },
      t = {
        -- setting a mapping to false will disable it
        -- ["<esc>"] = false,
      },
      v = {
        [">"] = { ">gv" },
        ["<"] = { "<gv" },
      },
    },
  },
}
