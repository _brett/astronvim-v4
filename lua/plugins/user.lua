-- You can also add or configure plugins by creating files in this `plugins/` folder
-- Here are some examples:

---@type LazySpec
return {

  -- Disable default plugins:
  { "max397574/better-escape.nvim", enabled = false },
  -- { "lewis6991/gitsigns.nvim", enabled = false },

  -- Conditionally enabled plugins:
  -- Used to override formatting and use bde-format
  { "stevearc/conform.nvim", enabled = vim.fn.has "mac" == 1 },
  -- Added Plugins:
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    -- config = function() require("lsp_signature").setup() end,
    opts = {},
  },
  { "tpope/vim-projectionist", lazy = false },
  { "tpope/vim-dispatch", lazy = false },
  { "terrortylor/nvim-comment" },
  { "ixru/nvim-markdown", ft = "markdown" },
  -- HTTP REST-Client Interface
  {
    "mistweaverco/kulala.nvim",
    -- config = function() require("kulala").setup() end,
    opts = {},
    ft = "http",
  },
  { "stevearc/dressing.nvim", opts = {} },
  { "ldelossa/nvim-dap-projects", ft = "cpp" },
  { "preservim/vimux", config = function() vim.cmd "let test#strategy = 'vimux'" end, lazy = false },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    opts = {},
    -- config = function() require("nvim-surround").setup() end,
  },
  {
    "echasnovski/mini.nvim",
    -- keys = {
    --   { "<leader>gp", "<cmd>silent lua MiniDiff.toggle_overlay()<cr>", desc = "Git Toggle Overlay" },
    -- },
    config = function()
      -- require("mini.diff").setup()
      require("mini.icons").setup()
      require("mini.misc").setup()
      require("mini.misc").setup_termbg_sync()
      require("mini.surround").setup()
      require("mini.trailspace").setup()
    end,
  },
  { "normen/vim-pio" },
  { "wsdjeg/vim-assembly" },
  { "brogers/6502-asm-vim-radical", dependencies = { "glts/vim-magnum" } },
  -- {
  --   "vzze/calculator.nvim",
  --   cmd = "Calculate",
  --   config = function()
  --     vim.api.nvim_create_user_command(
  --       "Calculate",
  --       'lua require("calculator").calculate()',
  --       { ["range"] = 1, ["nargs"] = 0 }
  --     )
  --   end,
  -- },
  -- { "jbyuki/quickmath.nvim" },
  -- { "vim-scripts/bccalc.vim" },
  { "unikuragit/vim-dirdiff" },
}
