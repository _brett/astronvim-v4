SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-print-directory

.DEFAULT_GOAL = help

REQUIREMENTS_TXT = requirements.txt
REQUIREMENTS_IN = requirements.in
MARKER=.initialized
VENV ?= .venv
PYTHON3 = $(shell which python3)
PYTHON = $(VENV)/bin/python
PIP = $(VENV)/bin/pip
PIP_SYNC = $(VENV)/bin/pip-sync
PIP_COMPILE = $(VENV)/bin/pip-compile

DESTINATION = pi@pidesk.local:/home/pi/projects/dashboard

.PHONY: clean-logs
clean-logs: ## Truncate log files
	@if [[ -d logs ]]; then
	echo "Truncating logs"
	for log in `ls logs`; do cat /dev/null > logs/$$log; done
	fi

.PHONY: clean-cache
clean-cache:
	@echo "Removing pycache"
	@find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

.PHONY: clean-venv
clean-venv:
	@echo "Removing $(VENV)"
	@rm -rf $(VENV)

.PHONY: clean
clean: ## Remove cache, logs, and venv
clean: clean-cache clean-logs clean-venv

$(REQUIREMENTS_IN):
	echo "-r $$HOME/.dotfiles/nvim/requirements-nvim.txt" > $(REQUIREMENTS_IN)

$(REQUIREMENTS_TXT): $(REQUIREMENTS_IN)
	$(PIP_COMPILE) $(REQUIREMENTS_IN)

$(VENV):
	$(PYTHON3) -m venv $(VENV)
	$(PIP) install wheel setuptools pip-tools

$(VENV)/$(MARKER): $(VENV) $(REQUIREMENTS_TXT)
	$(PIP_SYNC) $(REQUIREMENTS_TXT)
	touch $(VENV)/$(MARKER)

.PHONY: venv
venv: $(VENV)/$(MARKER)

.PHONY: run
run: ## Start App
run: venv
	$(PYTHON) main.py

.PHONY: dev-shell
dev-shell: ## Start a shell with venv activated
dev-shell: venv
	(source $(VENV)/bin/activate && exec $$SHELL)

.PHONY: sync
sync: ## Sync to $(DESTINATION)
	rsync -avz \
	--exclude $(VENV) \
	--exclude logs \
	--exclude .git \
	--exclude __pycache__ \
	--delete-before \
	. $(DESTINATION)

.PHONY: help
help: ## Show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

#  vim: set ft=make ts=8 sts=0 sw=0 tw=0 foldlevel=0 foldmethod=marker noet :
