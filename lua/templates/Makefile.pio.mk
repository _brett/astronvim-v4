SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-print-directory

.DEFAULT_GOAL = help

BAUD := 9600
ENVIRONMENT ?= uno # uno leonardo megaatmega2560 lilygo-t-display

.PHONY: init
init: ## Initalize project
init:
	pio -f -c vim init --board=$(ENVIRONMENT)


.PHONY: build
build: ## Build project without auto-uploading
build:
	pio -f -c vim run  --environment=$(ENVIRONMENT)

.PHONY: upload
upload: ## Build and upload (if no errors)
upload:
	pio -f -c vim run --target upload  --environment=$(ENVIRONMENT)

.PHONY: clean
clean: ## Clean compiled objects.
	pio -f -c vim run --target clean  --environment=$(ENVIRONMENT)

.PHONY: uploadfs
uploadfs: ## Upload SPIFFS image
	pio -f -c vim run --target uploadfs  --environment=$(ENVIRONMENT)

.PHONY: update
update: ## Update installed platforms and libraries via pio update.
	pio -f -c vim pkg update  --environment=$(ENVIRONMENT)

.PHONY: monitor
monitor: ## Monitor serial output
	pio -f -c vim device monitor --baud $(BAUD)  --environment=$(ENVIRONMENT)

.PHONY: help test all
help: ## Show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
