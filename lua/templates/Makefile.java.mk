SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-print-directory

.DEFAULT_GOAL = help

SRC  ?= src/main/java
TEST ?= src/test/java
MAIN ?= $(SRC)/Main.java
DST  ?= build/classes


.PHONY: clean
clean: ## Clean classes
	rm -f $$(find -name *.class)

$(DST)/%.class: $(SRC)/%.java
	@javac -d $(DST) $$(find $(SRC) -name *.java)

$(MAIN): setup

.PHONY: run
run: ## Run app
run: setup $(DST)/Main.class
	@java -cp $(DST) Main

setup: ## Initial project setup
setup: .setup-complete

define MAINDOC
public class Main {
        public static void main(String[] args) {
                System.out.println("Hello World, nice to meet you!");
        }
}
endef

export MAINDOC
.setup-complete:
	@mkdir -p $(DST)
	@mkdir -p $(SRC)
	@mkdir -p $(TEST)
	@echo -e "$$MAINDOC" > $(MAIN)
	@date > .setup-complete

.PHONY: help
help: ## Show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

#  vim: set ft=make ts=8 sts=0 sw=0 tw=0 foldlevel=0 foldmethod=marker noet :
