SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-print-directory

.DEFAULT_GOAL = help
ASM := cl65 -t cx16 
SIM := x16emu -scale 2 -rtc -debug -pastewarp -ram 2048 -prg
TARGET ?= main

PRG ?= $(TARGET).prg

.PHONY: config clean run help test all sim

%.prg: %.s
	$(ASM) $< -o $@

sim: $(PRG)
	$(SIM) $(PRG)
	tmux select-pane -t 1

run: ## Run example
run: clean sim

clean: ## Clean generated files
	$(RM) *.prg *.sym

help: ## Show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
