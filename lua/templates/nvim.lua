local vim = vim
local api = vim.api
-- local g = vim.g
-- local add_mark = require("user.global-marks").add

-- vim.keymap.set("n", "<cr>", function()
-- 	local current_dir = vim.fn.expand("%:p:h")
-- 	return ":wa|silent !tmux send-keys -t :1.3 'cd " .. current_dir .. " && make run' c-m<cr>"
-- end, { noremap = true, silent = true, expr = true })

-- api.nvim_set_keymap(
-- 	"n",
-- 	"<cr>",
-- 	":silent !tmux send-keys -t test-svc 'test-unit bqlacisvcgn_requesthandler_util' C-m<CR>",
-- 	{ noremap = true, silent = true }
-- )
-- api.nvim_set_keymap("n", "<cr>", ":wa|silent call VimuxRunCommand('make build')<cr>", { noremap = true, silent = true })

-- api.nvim_set_keymap(
--   "n",
--   "<leader>r",
--   ":wa|silent call VimuxRunCommand('make upload')<cr>",
--   { noremap = true, silent = true }
-- )

-- Compile and run current file
-- vim.keymap.set("n", "<cr>", function()
-- 	return ':wa|silent call VimuxRunCommand("clang++ -o main ' .. vim.api.nvim_buf_get_name(0) .. ' && ./main")<cr>'
-- end, { noremap = true, silent = true, expr = true })

-- [[ Code Blocks ]]
-- {{{
api.nvim_create_augroup("mdReport", { clear = true })
api.nvim_create_autocmd("FileType", {
  desc = "Set md report settings",
  group = "mdReport",
  pattern = "markdown",
  callback = function()
    local bufnr = api.nvim_get_current_buf()
    api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
    api.nvim_buf_set_keymap(bufnr, "n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
    api.nvim_buf_set_keymap(bufnr, "n", "<F6>", ":ClearResults<cr>", { noremap = true, silent = true })
    api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
    -- inline
    api.nvim_set_var("insert_after_fence", 0)
    api.nvim_set_var("code_blocks_result_begin", "")
    api.nvim_set_var("code_blocks_result_end", "")

    -- after code block fence
    -- api.nvim_set_var("insert_after_fence", 1)
    -- api.nvim_set_var("code_blocks_result_begin", "```json")
    -- api.nvim_set_var("code_blocks_result_end", "```")

    api.nvim_set_var("code_blocks_comment_result", 0)
    vim.cmd [[set commentstring=<!--\ %s\ -->]]
  end,
})
-- }}}

--[[ Fix <CR> in Quick Fix window ]]
-- {{{
vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "qf" },
  callback = function() vim.keymap.set("n", "<cr>", "<cr>", { noremap = true }) end,
})
-- }}}
