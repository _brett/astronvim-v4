local vim = vim
local api = vim.api
-- local g = vim.g
-- local add_mark = require("user.global-marks").add

vim.keymap.set("n", "<cr>", function()
	local current_file = vim.fn.expand("%:r")
	local cmd = ":wa|silent call VimuxRunCommand('TARGET="
		.. current_file
		.. " make run')<cr>"
		.. ":silent !tmux select-pane -t 2<cr>"
	return cmd
end, { noremap = true, silent = true, expr = true })

-- api.nvim_set_keymap(
-- 	"n",
-- 	"<cr>",
-- 	":silent !tmux send-keys -t test-svc 'test-unit bqlacisvcgn_requesthandler_util' C-m<CR>",
-- 	{ noremap = true, silent = true }
-- )

-- api.nvim_set_keymap(
-- 	"n",
-- 	"<cr>",
-- 	":wa|silent call VimuxRunCommand('make run')<cr>:silent !tmux select-pane -t 2<cr>",
-- 	{ noremap = true, silent = true }
-- )

-- api.nvim_set_keymap(
--   "n",
--   "<leader>r",
--   ":wa|silent call VimuxRunCommand('make upload')<cr>",
--   { noremap = true, silent = true }
-- )

-- Compile and run current file
-- vim.keymap.set("n", "<cr>", function()
-- 	return ':wa|silent call VimuxRunCommand("clang++ -o main ' .. vim.api.nvim_buf_get_name(0) .. ' && ./main")<cr>'
-- end, { noremap = true, silent = true, expr = true })

--[[ Fix <CR> in Quick Fix window ]]
vim.api.nvim_create_autocmd({ "FileType" }, {
	pattern = { "qf" },
	callback = function()
		vim.keymap.set("n", "<cr>", "<cr>", { noremap = true })
	end,
})
