local M = {}

M.set_global_mark = function(mark, filename, line_nr)
  vim.fn.setpos("'" .. mark, { vim.fn.bufnr(filename, 1), line_nr, 1, 0 })
end

M.setup = function()
  local mark = M.set_global_mark
  mark("A", "/Users/brogers24/.config/aerospace/aerospace.toml", 1)
  mark("C", "CMakeLists.txt", 1)
  mark("D", ".scratch/database.sql", 4)
  mark("E", "scripts/.local-environment", 1)
  mark("G", "/Users/brogers24/.config/ghostty/config", 1)
  mark("H", ".hammerspoon/init.lua", 4)
  mark("K", "/Users/brogers24/.config/kitty/kitty.conf", 26)
  mark("L", "scripts/local-environment", 26)
  mark("N", ".nvim.lua", 4)
  mark("M", "Makefile", 1)
  mark("P", ".projections.json", 4)
  mark("Q", "/Users/brogers24/.dotfiles/qmk_firmware/qmk_firmware/keyboards/crkbd/keymaps/brogers/keymap.c", 83)
  mark("R", ".scratch/requests.http", 4)
  mark("S", ".scratch/scratch.md", 4)
  mark("T", "/Users/brogers24/.tmux.conf.local", 350)
  mark("U", "/Users/brogers24/.config/nvim/lua/user/plugins/user.lua", 1)
  mark("V", "/Users/brogers24/.config/nvim/lua/polish.lua", 1)
  mark("W", "/Users/brogers24/.config/wtf/config.yml", 1)
  mark("Z", "/Users/brogers24/.config/zsh/.zshrc", 4)
end

M.add = function(mark, filename, line_nr)
  line_nr = line_nr or 1
  if line_nr == 0 then line_nr = 1 end
  M.set_global_mark(mark, filename, line_nr)
end

return M
